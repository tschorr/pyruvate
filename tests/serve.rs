use libc;
use std::thread;
use std::time::Duration;
use cpython::Python;
use pyruvate;

#[test]
fn test_serve() {
    let gil = Python::acquire_gil();
    let py = gil.python();
    thread::spawn(move || {
        thread::sleep(Duration::from_secs_f32(0.5));
        unsafe {
            libc::raise(libc::SIGINT);
        }
    });
    // serve in main thread
    match pyruvate::serve(py, py.None().as_ptr(), Some("localhost:0".to_string()), 2, 16, true, false, 0, 0, None, 60) {
        Ok(_) => (),
        Err(_) => assert!(false),
    }
}
